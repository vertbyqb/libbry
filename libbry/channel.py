"""
channel.py --- Operate on LBRY Channels

Copyright (C) 2021 libbry contributors

This file is part of libbry, a Python 3 library for accessing the LBRY network.

libbry is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

libbry is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with libbry. If not, see <https://www.gnu.org/licenses/>.

Commentary:

Contains functions related to channels (e.g. listing claims, creating and listing channels in the local wallet, etc.)
"""

import subprocess as subp
import json
from contextlib import suppress
from typing import Union

from . import utils as u


def list_claims(
    channel: str = "",
    channel_id: str = "",
    order_by: str = "release_time",
    reverse: bool = False,
    page: Union[int, str] = 1,
    page_size: Union[int, str] = 20,
    no_totals: bool = False,
    settings: dict = u.default_settings,
) -> dict:
    """
    List the claims signed by the given channel.

    Keyword arguments:
    channel_id -- the claim ID of the channel. Takes priority over *channel* if given.
    channel -- the URL or name of the channel
    order_by -- how to order the results. See order_by in the SDK docs <https://lbry.tech/api/sdk#claim_search> for possible values.
    reverse -- reverse the order of the results
    page -- page of results to show
    page_size -- size of each page of results
    no_totals -- do not calculate the total number of pages and items in result set (significant performance boost)
    settings -- settings to use
    """
    s = settings

    return_value = {"items": []}

    call = [
        s["lbrynet_binary"],
        "claim",
        "search",
        "--order_by=" + ("^" if reverse else "") + order_by,
        "--page=" + str(page),
        "--page_size=" + str(page_size),
    ]
    if channel_id:
        call.append("--channel_ids=" + channel_id)
    elif channel:
        call.append("--channel=" + channel)
    else:
        raise ValueError("Must give either channel_id or channel")

    if no_totals:
        call.append("--no_totals")

    out = subp.check_output(call)
    out = u.load_json(out)

    l = ["page", "page_size"]
    if not no_totals:
        l += ["total_items", "total_pages"]
    for i in l:
        return_value[i] = out[i]

    out = out["items"]

    for item in out:
        return_value["items"].append(u.sort_claim_info(item, s))

    return return_value
