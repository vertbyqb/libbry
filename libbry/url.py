"""
url.py --- Operate on LBRY URLs

Copyright (C) 2021 libbry contributors

This file is part of libbry, a Python 3 library for accessing the LBRY network.

libbry is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

libbry is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with libbry. If not, see <https://www.gnu.org/licenses/>.

Commentary:

This file contains functions for getting information about
(e.g. resolving, reactions, etc.) and performing operations with
(e.g. downloading) LBRY URLs and claim ids.
"""

import urllib.parse
from datetime import datetime
import subprocess as subp
import json
import os
from contextlib import suppress

from . import utils as u


def clean_up_url(url: str) -> str:
    """Cleans up a given LBRY URL to make it usable"""

    ##### Converting a HTTPS domain ( of any website ) into a LBRY url ####

    # Any variation of this:
    ## https://odysee.com/@blenderdumbass:f/hacking-fastlbry-live-with-you:6
    ## customlbry.com/@blenderdumbass:f/hacking-fastlbry-live-with-you:6
    # Should become this:
    ## lbry://@blenderdumbass:f/hacking-fastlbry-live-with-you:6

    if "/" in url and not url.startswith("@") and not url.startswith("lbry://"):
        if "@" in url:
            url = url[url.find("@") :]
        else:
            url = url[url.rfind("/") :].strip("/")

        # Some web interfaces pass data through URLs using a '?'.
        # This is not valid for a LBRY URL, so we remove everything
        # after it to avoid errors.
        url = url.split("?")[0]

        url = "lbry://" + url

    # Replace all but first instance of ":" (in "lbry://") with "#"
    url = url.replace(":", "#").replace("#", ":", 1)

    return url


def get_url_info(url: str, settings: dict = u.default_settings) -> dict:
    """Gets metadata about URLs or claim ids.

    Keyword arguments:
    url -- the URL or claim ID to operate on
    settings -- settings to use
    """

    s = settings

    url = clean_up_url(url)

    # If a url looks like it might be a claim id, try to get the claim from it
    if len(url) == 40 and not url.startswith("lbry://"):
        x = subp.check_output(
            [s["lbrynet_binary"], "claim", "search", "--claim_id=" + url]
        )
        x = u.load_json(x)

        if len(x["items"]) == 1:
            # If "items" has something in it we know it was a claim id
            out = x["items"][0]
            url = out["canonical_url"]

    else:
        out = subp.check_output([s["lbrynet_binary"], "resolve", url])
        out = u.load_json(out)
        out = out[url]

    # Give an error if nothing was returned
    if "value_type" not in out:
        raise ValueError("The string given was not a valid URL or claim ID")

    # Add the lbry:// just in case
    if not url.startswith("lbry://"):
        url = "lbry://" + url

    return u.sort_claim_info(out, s)


def download(
    url: str = "",
    claim_id: str = "",
    download_path: str = "",
    download_directory: str = "",
    file_name: str = "",
    settings: dict = u.default_settings,
) -> dict:
    """
    Download a claim from a url or claim ID.

    Keyword arguments:
    url -- URL to download
    claim_id -- claim ID to download
    download_path -- path of the file or directory to download to
    download_directory -- directory to download to
    file_name -- name of file to download to (will download into default directory if download_directory is not given)
    settings -- settings to use
    """
    s = settings

    if download_path:
        download_path = os.path.expanduser(download_path)
        download_directory = os.path.dirname(download_path)
        file_name = os.path.basename(download_path)
    elif download_directory:
        download_directory = os.path.expanduser(download_directory)

    if not url and not claim_id:
        raise ValueError("Must give either a URL or a claim ID")
    elif url:
        uri = url
    elif claim_id:
        out = subp.check_output(
            [
                s["lbrynet_binary"],
                "claim",
                "search",
                "--claim_id=" + claim_id,
                "--no_totals",
            ]
        )
        out = u.load_json(out)

        uri = out["items"][0]["permanent_url"]

    out = [s["lbrynet_binary"], "get", uri]

    for v in ("download_directory", "file_name"):
        value = locals()[v]
        if value:
            out.append("--" + v + "=" + value)

    out = subp.check_output(out)
    out = u.load_json(out)

    return sort_claim_status(out)


def delete_from_claim_id(
    claim_id: str,
    delete_from_download_dir: bool = True,
    delete_all: bool = True,
    settings: dict = u.default_settings,
) -> bool:
    """
    Deletes a downloaded claim by its claim ID. Returns True if successful, false if not.

    Keyword arguments:
    claim_id -- ID of claim to delete
    delete_from_download_dir = delete file from download directory, instead of just deleting blobs
    delete_all -- if there are multiple matching files, allow the deletion of multiple files. Otherwise do not delete anything.
    settings -- settings to use
    """
    s = settings

    call = [s["lbrynet_binary"], "file", "delete", "--claim_id=" + claim_id]
    # If these are given as parameters, pass them to lbrynet
    for a in ("delete_from_download_dir", "delete_all"):
        if locals()[a]:
            call.append("--" + a)

    # If the SDK succeeds it returns "true", so we can just compare to that.
    out = subp.check_output(call).decode().rstrip()
    out = out == "true"
    return out


def delete_from_url(
    url: str,
    delete_from_download_dir: bool = True,
    delete_all: bool = True,
    settings: dict = u.default_settings,
) -> dict:
    """
    Deletes a downloaded claim by its URL.

    Keyword arguments:
    url -- URL to delete
    delete_from_download_dir = delete file from download directory, instead of just deleting blobs
    delete_all -- if there are multiple matching files, allow the deletion of multiple files. Otherwise do not delete anything.
    settings -- settings to use
    """
    s = settings

    url = clean_up_url(url)

    # lbrynet's file delete call only accepts a claim id or name, so we will try to get the claim id first.
    if "#" in url:
        cid = url.split("#")[-1]
        if len(cid) == 40:
            return delete_from_claim_id(
                cid,
                delete_from_download_dir=delete_from_download_dir,
                delete_all=delete_all,
                settings=settings,
            )

    # If we can't get the claim id we will get the name from the URL
    name = url.strip("lbry://")
    name = name.split("/")[-1]
    name = name.split("#")[0]

    call = [s["lbrynet_binary"], "file", "delete", "--claim_name=" + name]
    for a in ("delete_from_download_dir", "delete_all"):
        if locals()[a]:
            call.append("--" + a)

    # If the SDK succeeds it returns "true", so we can just compare to that.
    out = subp.check_output(call).encode() == "true"
    return out


def status_from_claim_id(claim_id: str, settings: dict = u.default_settings) -> dict:
    """
    Gets the progress of a downloading claim by its ID.
    """
    # TODO: Get status from URL too.

    s = settings

    out = subp.check_output(
        [s["lbrynet_binary"], "file", "list", "--claim_id=" + claim_id]
    )
    out = u.load_json(out)

    try:
        out = out["items"][0]
    except:
        # TODO: Split the causes for this (download not started, claim
        # has nothing to download, improper claim ID, etc.) into
        # separate exceptions.
        raise u.LibbryException(
            "Could not check the status of the given claim. Check that the download is started."
        )

    return sort_claim_status(out)


def sort_claim_status(out: dict) -> dict:
    """
    Returns the important values from the output lbrynet gave us (e.g. from `lbrynet get`)

    Keyword arguments:
    out -- the JSON decoded output from lbrynet
    """

    # Since each key is the same in the return_value and out we can iterate through a list of keys
    bytes_keys = ["total_bytes", "written_bytes"]
    # TODO: Add more keys
    keys = bytes_keys + [
        "download_path",
        "download_directory",
        "file_name",
        "streaming_url",
    ]
    return_value = {}

    for key in keys:
        try:
            return_value[key] = out[key]
            if key in bytes_keys:
                return_value[key + "_h"] = u.csize(out[key])
        except:
            pass

    # And then progress, which we can't do the same way because we need to divide two previous values
    try:
        return_value["progress"] = out["written_bytes"] / out["total_bytes"]
    except:
        pass

    return return_value
