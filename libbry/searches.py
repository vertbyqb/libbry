"""
searches.py --- performs searches on the LBRY network

Copyright (C) 2021 libbry contributors

This file is part of libbry, a Python 3 library for accessing the LBRY network.

libbry is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

libbry is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with libbry. If not, see <https://www.gnu.org/licenses/>.
"""

from . import utils as u
import subprocess as subp
from contextlib import suppress


def simple(
    query: str,
    page: int = 1,
    page_size: int = 20,
    order_by: str = "release_time",
    no_totals: bool = False,
    settings: dict = u.default_settings,
) -> dict:
    """Performs a search on the LBRY network.

    Returns a dict with the status and a list of results.

    Keyword arguments:
    query -- search query
    page -- page of results to show
    page_size -- size of each page of results
    order_by -- how to order the results. See order_by in the SDK docs <https://lbry.tech/api/sdk#claim_search> for possible values.
    no_totals -- do not calculate the total number of pages and items in result set (significant performance boost)
    settings -- settings to use
    """

    s = settings
    return_value = {"items": []}

    call = [
        s["lbrynet_binary"],
        "claim",
        "search",
        '--text="' + query + '"',
        "--page=" + str(page),
        "--page_size=" + str(page_size),
        "--order_by=" + order_by,
    ]

    if no_totals:
        call.append("--no_totals")

    out = subp.check_output(call)
    out = u.load_json(out)

    l = ["page", "page_size"]
    if not no_totals:
        l += ["total_items", "total_pages"]
    for i in l:
        return_value[i] = out[i]

    out = out["items"]

    for item in out:
        return_value["items"].append(u.sort_claim_info(item, s))

    return return_value
