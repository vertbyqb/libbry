"""
__init__.py --- Simplifies calls to common functions

Copyright (C) 2021 libbry contributors

This file is part of libbry, a Python 3 library for accessing the LBRY network.

libbry is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

libbry is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with libbry. If not, see <https://www.gnu.org/licenses/>.

Commentary:

Imports some functions into the base package.
So instead of running:
  libbry.searches.simple()
You can run:
  libbry.search()
"""

import libbry.channel

from .searches import simple as search
from .url import get_url_info as resolve
from .url import download as get
