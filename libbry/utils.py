"""
utils.py --- common variables and functions for libbry.

Copyright (C) 2021 libbry contributors

This file is part of libbry, a Python 3 library for accessing the LBRY network.

libbry is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

libbry is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with libbry. If not, see <https://www.gnu.org/licenses/>.
"""

from datetime import datetime
from datetime import timedelta
from typing import Union
import json

# The default settings each function will use
default_settings = {"lbrynet_binary": "lbrynet", "timeformat": "%a, %d %b %Y %X %z"}

# Dictionary of status codes and their names
status_codes = {
    "UNKNOWN": -1,
    "SUCCESS": 0,
    "INVALID_JSON": -59964,
    "NOT_A_URL": -45788,
    "PARAMETER_ERROR": -31884,
}

# Gives an error saying the JSON is invalid.
# Most often this is because the lbrynet daemon isn't started
def invalid_json_error(return_value: dict) -> dict:
    return_value["error"] = (
        "lbrynet returned invalid JSON" + "\n" + "Ensure the daemon is running"
    )
    return_value["code"] = status_codes["INVALID_JSON"]
    return return_value


def no_error(return_value: dict) -> dict:
    # return_value.pop("error", None)
    return_value["error"] = None
    return_value["code"] = status_codes["SUCCESS"]
    return return_value


# A dictionary for converting the file types returned by lbrynet to more understandable names.
## utils.file_type_translations["stream"]
file_type_translations = {
    "stream": "file",
    "repost": "shared",
    "channel": "channel",
    "collection": "playlist",
    "video": "video",
    "audio": "sound",
    "document": "text",
    "binary": "file",
    "image": "picture",
}


def csize(x: Union[int, str]) -> str:
    """Converts bytes to human-readable sizes as a str."""

    # TODO: Support decimal bytes (units of 10 instead of 2)

    x = float(x)

    l = ["B", "KiB", "MiB", "GiB", "TiB"]

    for i in range(5):
        if x > 1024:
            x = x / 1024
        else:
            return str(round(x, 2)) + " " + l[i]
    return str(round(x, 2)) + " " + l[i]


def timestring(seconds: int) -> str:
    """Converts a time in seconds to days, hours, minutes, seconds, and microseconds (if any)"""
    return str(timedelta(seconds=seconds))


def sort_claim_info(claim: dict, settings: dict = default_settings) -> dict:
    """
    Gets the needed information from a claim that lbrynet gives us.

    Some values in the return value may be excluded because they could
    not be found. Additionally, the whole return value may be enclosed
    in a "reposted_claim" value if the claim is a repost.

    Keyword arguments:
    claim -- a dictionary containing metadata about a claim (e.g. from lbrynet resolve)
    settings -- settings to use

    Return value:
    {
        # Key                  Description
        "name":              "Name of the claim",
        "url":               "LBRY URL of the claim",
        "claim_id":          "Claim ID of the claim",
        "title":             "Title of the claim (if any)",
        "description":       "Description of the claim (if any)",
        "channel":           "Name of the signing channel (if any)",
        "channel_id":        "Claim ID of the signing channel (if any)",
        "release_time":      "Release time as a datetime.datetime object",
        "release_time_unix": "Release time in seconds since the Unix Epoch",
        "release_time_h":    "Release time formatted in the format *timeformat*",
        "media_type":        "Media type of the source content (if any)",
        "address":           "Claim address",
        "amount":            "Claim amount",
        "confirmations":     "Number of confirmations the claim has received on the blockchain ",
        "height":            "Claim height",
        "reposted":          "The number of times the claim has been reposted",
        "license":           "The license the claim is under (if any)",
        "license_url":       "The URL to the license the claim is under (if any)",
        "file_name":         "File name of the source file",
        "sd_hash":           "SD hash of the source file",
        "file_size":         "File size of the source file in bytes",
        "file_size_h":       "File size of the source file in a human readable format",
        "stream_type":       "The stream type of the stream",
        "tags":              "List of tags on the video",
        "thumbnail_url":     "URL of the thumbnail of the claim",
        "video_duration":    "Duration of the video in seconds (if applicable)",
        "video_duration_h":  "Duration of the video in a human readable format (if applicable)",
        "video_height":      "Height of the video in pixels (if applicable)",
        "video_width":       "Width of the video in pixels (if applicable)",
        "value_type":        "Value type of the claim",
        "fee":               "Fee of the claim, in fee_currency (if any)",
        "fee_currency":      "Currency of the claim's fee (if any)",
        "fee_address":       "Address of the claim's fee (if any)",
    }
    """
    # TODO: To reduce the size of the docstring and redundancy, we may
    # want to move the Return value section of the docstring to a part
    # of the application documentation.

    s = settings
    timeformat = s["timeformat"]

    if "reposted_claim" in claim:
        claim = claim["reposted_claim"]
        is_repost = True
    else:
        is_repost = False

    # Contains the keys for the returned dictionary, the keys to get them from *claim*, and (optionally) functions to pass them into.
    pairs = {
        "name": [["name"]],
        "url": [["canonical_url"]],
        "claim_id": [["claim_id"]],
        "title": [["value", "title"]],
        "description": [["value", "description"]],
        "channel_url": [
            ["signing_channel", "permanent_url"]
        ],  # The permanent URL contains both the name and the claim ID, and it's easy to separate them.
        "channel_title": [["signing_channel", "value", "title"]],
        "release_time_unix": [
            ["value", "release_time"],
            [int],
        ],  # This will take claim["value"]["release_time"], pass it into int(), and assign that to processed["release_time_unix"].
        "release_time": [
            ["value", "release_time"],
            [
                int,
                datetime.fromtimestamp,
            ],
        ],
        "media_type": [["value", "source", "media_type"]],
        "address": [["address"]],
        "amount": [["amount"], [float]],
        "confirmations": [["confirmations"]],
        "height": [["height"]],
        "reposted": [["meta", "reposted"]],
        "license": [["value", "license"]],
        "license_url": [["value", "license_url"]],
        "file_name": [["value", "source", "name"]],
        "sd_hash": [["value", "source", "sd_hash"]],
        "file_size": [["value", "source", "size"], [int]],
        "file_size_h": [["value", "source", "size"], [csize]],
        "stream_type": [["value", "stream_type"]],
        "tags": [["value", "tags"]],
        "thumbnail_url": [["value", "thumbnail", "url"]],
        "video_duration": [["value", "video", "duration"]],
        "video_duration_h": [["value", "video", "duration"], [timestring]],
        "video_height": [["value", "video", "height"]],
        "video_width": [["value", "video", "width"]],
        "value_type": [["value_type"]],
        "fee": [
            ["value", "fee", "amount"]
        ],  # TODO: It'd be nice to have these fee values in a dictionary, like the returned JSON does.
        "fee_currency": [["value", "fee", "currency"]],
        "fee_address": [["value", "fee", "address"]],
    }

    processed = {}

    for key in pairs:
        try:
            if len(pairs[key]) == 2:
                processed[key] = chain_functions(
                    get_path(claim, pairs[key][0]), pairs[key][1]
                )
            else:
                processed[key] = get_path(claim, pairs[key][0])
        except:
            # If getting the value fails it will leave it undefined.
            # This will make it more obvious that a value couldn't be
            # found because it will fail with a KeyError as opposed to
            # passing an unexpected None to wherever it ends up going.
            pass

    # Because strftime is a function in the datetime.datetime object, we can't call it above.
    if "release_time" in processed:
        try:
            processed["release_time_h"] = processed["release_time"].strftime(timeformat)
        except:
            pass

    if is_repost:
        return_value = {"reposted_claim": processed}
    else:
        return_value = processed

    return_value["is_repost"] = is_repost

    return return_value


def get_path(dictionary: dict, branches: list):
    """
    Goes through each branch of a dictionary and returns the last branch.

    Keyword arguments:
    dictionary -- dictionary to go through
    branches -- keys (in order) to get the value of

    Example:

    dictionary = {
        "a": {
            "b": {
                "c": 1,
                "d": ["a", "b", "c"]
            },
            "e": 2
        },
        "f": 3
    }

    branches = ["a", "b"]

    When given these two values, it will return dictionary["a"]["b"], or:
    {
        "c": 1,
        "d": ["a", "b", "c"]
    }
    """
    current = dictionary
    for branch in branches:
        current = current[branch]
    return current


def chain_functions(function_input, functions: list):
    """
    Chains functions together, like a pipe. function_input goes into functions[0] and the output from that goes into functions[1] and so on.
    """
    i = function_input
    for function in functions:
        i = function(i)
    return i


class LibbryException(Exception):
    """Base exception for libbry exceptions"""

    message = "An unknown exception occurred"

    def __init__(self, message=None):
        if message == None:
            message = self.message
        super().__init__(message)


class InvalidJSONException(LibbryException):
    """lbrynet's JSON was invalid"""

    message = "lbrynet returned invalid JSON. Ensure the daemon is running."


def load_json(json_data: bytes):
    """Loads JSON Data and returns the results"""
    try:
        return json.loads(json_data)
    except json.decoder.JSONDecodeError:
        raise InvalidJSONException
    except:
        raise LibbryException
