# Deprecation

I am no longer working on this project, as there is another already much more powerful and easy alternative, [lyberry_api](https://git.tristans.cloud/tristan/lyberry_api). I recommend using that over this.

# <img src="logo.png" alt="libbry logo" width="45" height="45" /> libbry

`libbry` (pronounced "lib bee-are-why") is a wrapper Python library over the `lbrynet` SDK to provide usable output for programs interacting with the LBRY network. `libbry` takes the work out of parsing the output of `lbrynet` for you, allowing application developers to focus on the beauty of the program.

## To Do list

Here is a list of features that are implemented and to come. If you have a feature you want added you can file it in the [issue tracker](https://codeberg.org/vertbyqb/libbry/issues). There are also additional feature requests/bugs there that are not here.

- [x] Search
- [x] Get information about URLs
- [x] Download publications
- [x] Get recent publications from channels
- [ ] See following and trending ([issue #3](https://codeberg.org/vertbyqb/libbry/issues/3))
- [ ] Login ([issue #14](https://codeberg.org/vertbyqb/libbry/issues/14))
- [ ] Upload new publications ([issue #4](https://codeberg.org/vertbyqb/libbry/issues/4))
- [ ] Get wallet balance and history ([issue #5](https://codeberg.org/vertbyqb/libbry/issues/5))
- [ ] View and publish comments ([issue #6](https://codeberg.org/vertbyqb/libbry/issues/6))
- [ ] Make tests ([issue #9](https://codeberg.org/vertbyqb/libbry/issues/9))
- [ ] Make contributing guidelines ([issue #10](https://codeberg.org/vertbyqb/libbry/issues/10))
- [ ] Give actual exceptions when errors occur ([issue #11](https://codeberg.org/vertbyqb/libbry/issues/11))

## Copy~~right~~left

`libbry` is copyright © Vertbyqb and other contributors 2021-2022 and is available under the [GNU General Public License version 3](LICENSE.md) or, at your option, any later version of the license as published by the Free Software Foundation. Parts of libbry are adapted from [FastLBRY Terminal](https://notabug.org/jyamihud/FastLBRY-terminal), which is available under the same license and is copyright © J.Y.Amihud and other contributors 2021.

Documentation files (files with the `md` file extension) are licensed under the [GNU Free Documentation License version 1.3](fdl-1.3.md) or, at your option, any later version of the license as published by the Free Software Foundation. Code fragments in the documentation (typically surrounded by backticks (`` ` ``); see [here](https://web.archive.org/web/20211023185043/https://www.freecodecamp.org/news/how-to-format-code-in-markdown/) for more details) are additionally distributable under the GNU General Public license under the same terms mentioned at the start of the last paragraph.

The libbry logo (`logo.png` and `logo.blend`) is copyright © January 2022 TrueAuraCoral (@zortazert) and is released by him under the [Creative Commons Attribution-ShareAlike 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license.

The `libbry` library depends on the [LBRY SDK](https://github.com/lbryio/lbry-sdk) (not included), which is copyright © 2015-2022 LBRY Inc. and is available under the [Expat license](https://raw.githubusercontent.com/lbryio/lbry-sdk/master/LICENSE).
