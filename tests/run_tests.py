from search import *

colors = {"GREEN": "\033[92m", "RED": "\033[91m", "NORM": "\033[0m"}

if test_search():
    print(colors["GREEN"] + "✓ Search test succeeded" + colors["NORM"])
