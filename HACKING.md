Note: This is a work in progress

# Guidelines for code

To increase the likelihood that your code will be accepted, kindly follow these guidelines.

- Before committing code, format it with the [black](https://github.com/psf/black) code formatter. You may want to configure your editor to automatically format the code for you. In [GNU Emacs](https://gnu.org/software/emacs) you can use the [python-black](https://github.com/wbolster/emacs-python-black) package.
- Use [type hints](https://www.python.org/dev/peps/pep-0484/#type-definition-syntax) in functions when possible.
